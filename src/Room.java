import java.util.List;

public class Room {
    private int number;
    private List<Student> students;

    public Room(int number, List<Student> students) {
        this.number = number;
        this.students = students;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public void addStudent(Student student){

        students.add(student);
    }
    public void removeStudent(Student student){
        students.remove(student);
    }
}
