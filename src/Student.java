public class Student extends User{
    private String courseName;

    public Student(int id, String lastName, String firstName, String courseName) {
        super(id, lastName, firstName);
        this.courseName = courseName;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }
}
