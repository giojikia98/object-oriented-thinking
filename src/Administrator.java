import java.util.List;

public class Administrator extends User{
    private List<Dormitory> dormitories;


    public Administrator(int id, String lastName, String firstName, List<Dormitory> dormitories) {
        super(id, lastName, firstName);
        this.dormitories = dormitories;
    }

    public List<Dormitory> getDormitories() {
        return dormitories;
    }
}
